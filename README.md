# hong-h5-form


<span style="display:none;">[博客地址](https://blog.csdn.net/girafee/article/details/110866812)</span>

# 更新日志

## 1.4.6
### 2022-03-01
#### 更新
- 新增 hong-upload组件native属性，将支持混合开发时上传图片由APP自行决定
- 新增 hong-upload组件Events @js-to-native 与native配合使用
- 更新 hong-upload组件，当upload Attributes没有配置method时，组件将在done回调中返回File对象
> 以上同步hong-form配置


## 1.4.5
### 2022-02-28
#### 更新
- 更新 formslot=true时，custom将作用于根元素

## 1.4.4
### 2022-01-06
#### 更新
- 新增 hong-scroll-view组件 ，[示例](/doc/other/scroll-view)

## 1.4.2
### 2021-11-16
#### 更新
- 优化 修复picker组件多级联动时一个已知问题

## 1.4.0
### 2021-10-18
#### 更新
- 新增 hong-form#type=caption 新增属性isHtml，将支持html元素渲染
- 新增 hong-form#type=upload 新增属性done，用于上传成功后回调
- 修改 hong-form options 通用属性 dicResponseFormat 更名为 responseFormat （主要应用于picker、select、radio、check、upload）

## 1.3.9
### 2021-10-13
#### 更新
- 新增 hong-form options通用属性新增dicResponseFormat 可自定义指定响应参数（原来默认使用接口data字段）

## 1.3.8
### 2021-10-12
#### 更新
- 新增 hong-form#type=multi-record 新增属性：noTitle、noBtn 目的渲染多记录数据且不可增删
- 优化 hong-form#type=caption 新增属性：dic、append、dataFrom 数据来源可由label配置、name字段对应数据、dic字典决定

## 1.3.7
### 2021-09-24
#### 更新
- 优化 为input、textarea组件提供失焦blur回调
- 优化 修复一个低级错误，placeholder拼写错误

## 1.3.5
### 2021-07-22
#### 更新
- 新增 其他组件 hong-count-up ，[示例](/doc/other/count-up)
- 优化 实现textarea必填时标题加上<font color="red">*</font>号，并且errorTip为true时会有页面提示。

## 1.3.4
### 2021-07-22
#### 更新
- 新增 表单组件type新增check、switch类型，单组件 [hong-check](/doc/component/check)、[hong-switch](/doc/component/switch)。
- 优化 表单校验方法，[示例](/doc/form/valid)
- 修复 hong-radio、hong-upload校验不提示问题

## 1.3.3
### 2021-07-16
#### 更新
- 新增 全局api $emitter、setPx，[示例](/doc/api)
- 新增 表单组件type新增radio类型[示例](/doc/form/form-doc)，单组件 hong-radio ，[示例](/doc/form/radio)
- 新增 其他组件 hong-grid ，[示例](/doc/other/grid)
- 新增 详情组件（当type=image-list）、单组件hong-image-list height属性，可自定义图片高度，[文档](/doc/form-details/form-details-doc) 

## 1.3.2
### 2021-06-29
#### 更新
- 新增 hong-form-details tab、tabActive属性，[示例](/doc/form-details/tab)
- 新增 hong-form-details Scoped Slot[示例](/doc/form-details/form-details-doc)
- 新增 hong-image-list 单组件，[示例](/doc/component/detail/image-list)

## 1.3.1
### 2021-06-21
#### 更新
- 新增 hong-image 组件新增属性sizeType，[示例](/doc/form-details/image-size-type)
- 新增 全局api，[前往](/doc/api)

## 1.3.0
### 2021-06-18
#### 更新
- 新增 hong-form-details 组件，用于展示表单详情，[示例](/doc/form-details/form-details-doc)
- 新增 hong-cell-item 单组件，[示例](/doc/component/detail/cell-item)
- 新增 hong-field-item 单组件，[示例](/doc/component/detail/field-item)
- 新增 hong-image 单组件，[示例](/doc/component/detail/image)
- 新增 hong-recursion 单组件，[示例](/doc/component/detail/recursion)
- 新增 hong-single-recursion 单组件，[示例](/doc/component/detail/single-recursion)
- 新增 hong-sub-title 单组件，[示例](/doc/component/detail/sub-title)
- 新增 hong-value-field 单组件，[示例](/doc/component/detail/value-field)
- 新增 hong-value-tab 单组件，[示例](/doc/component/detail/value-tab)

- 更新 安装插件时支持传入主题色theme属性，默认#E50213
#### 说明
- 无

## 1.2.9
### 2021-06-16
#### 更新
- 新增picker组件的dicFormat属性
#### 说明
- 无

###### 联系
vx: <a>838727606</a>
个人作品，学习为主。如有建议，随时骚扰。
