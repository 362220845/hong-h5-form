// 导入组件，组件必须声明 name
import HongFormDetails from './main'

// 为组件添加 install 方法，用于按需引入
// HongForm.install = function (Vue) {
//   Vue.component(HongForm.name, HongForm)
// }
const formDetailsConfig = [HongFormDetails]

export default formDetailsConfig
