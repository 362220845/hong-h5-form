// 导入组件，组件必须声明 name
import HongTabsForm from './main'

// 为组件添加 install 方法，用于按需引入
// HongTabsForm.install = function (Vue) {
//   Vue.component(HongTabsForm.name, HongTabsForm)
// }
const tabsFormConfig = [HongTabsForm]

export default tabsFormConfig
