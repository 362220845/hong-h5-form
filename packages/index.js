// packages / index.js
import '@pkg/assets/custom.styl'
import { deepClone, getObjType, setPx } from '@pkg/utils/util'
import { validatenull } from '@pkg/utils/validate'
import { Emitter } from '@pkg/utils/controller'
// 导入单个组件
import UIConfig from './components/ui/index'
import DetailUIConfig from './components/detail-ui/index'
import OtherUIConfig from './components/other-ui/index'
// import MandConfig from './components/mand-mobile/index'

import formConfig from './form/index'
import formDetailsConfig from './form-details/index'
import tabsFormConfig from './tabs-form/index'

// 以数组的结构保存组件，便于遍历
const components = [
  ...UIConfig,
  ...DetailUIConfig,
  ...OtherUIConfig,
  ...formConfig,
  ...tabsFormConfig,
  ...formDetailsConfig
]
const prototypes = {
  deepClone,
  validatenull,
  getObjType,
  setPx,
  $emitter: new Emitter()
}

// 定义 install 方法
const install = function (Vue, opts = {}) {
  if (!Vue || install.installed) return
  install.installed = true
  // Vue.use(MandConfig)
  // Vue.use(UIConfig)
  const mandMobile = window['mand-mobile']
  if (mandMobile) {
    Vue.prototype.$toast = mandMobile.Toast
    Vue.prototype.$dialog = mandMobile.Dialog
    Vue.prototype.$actionsheet = mandMobile.ActionSheet
  } else {
    console.warn('未检测到mand-mobile')
  }
  // 遍历并注册全局组件
  components.map(component => {
    Vue.component(component.name, component)
  })

  Object.keys(prototypes).forEach((key) => {
    Vue.prototype[key] = prototypes[key]
  })

  document.documentElement.style.setProperty('--color-primary', opts.theme || '#e50213')
}

if (typeof window !== 'undefined' && window.Vue) {
  install(window.Vue)
}

// export {
//   HongForm,
//   HongTabsForm
// }
export default {
  // 导出的对象必须具备一个 install 方法
  install,
  version: require('../package.json').version
}
