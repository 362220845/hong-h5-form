import { validatenull } from './validate'
export function dateFormat (type) {
  const date1 = new Date()
  date1.setTime(date1.getTime() - type * 24 * 60 * 60 * 1000)
  const _year = date1.getFullYear()
  const _month = date1.getMonth() + 1
  const _day = date1.getDate()
  const s = `${_year}-${_month > 9 ? _month : `0${_month}`}-${_day > 9 ? _day : `0${_day}`}`
  return s
}

export function substrPhone (phone) {
  if (!phone || phone.length !== 11) {
    return phone
  }
  return phone.substr(0, 3) + '****' + phone.substr(7, 10)
}
export function substrIdcard (idcard) {
  const length = idcard && idcard.length
  if (!idcard || (length !== 15 && length !== 18)) {
    return idcard
  }
  if (length === 15) {
    return idcard.substr(0, 12) + '****'
  }
  if (length === 18) {
    return idcard.substr(0, 12) + '******'
  }
}

export const getObjType = obj => {
  var toString = Object.prototype.toString
  var map = {
    '[object Boolean]': 'boolean',
    '[object Number]': 'number',
    '[object String]': 'string',
    '[object Function]': 'function',
    '[object Array]': 'array',
    '[object Date]': 'date',
    '[object RegExp]': 'regExp',
    '[object Undefined]': 'undefined',
    '[object Null]': 'null',
    '[object Object]': 'object'
  }
  if (obj instanceof Element) {
    return 'element'
  }
  return map[toString.call(obj)]
}

/**
 * 对象深拷贝
 */
export const deepClone = data => {
  var type = getObjType(data)
  var obj
  if (type === 'array') {
    obj = []
  } else if (type === 'object') {
    obj = {}
  } else {
    // 不再具有下一层次
    return data
  }
  if (type === 'array') {
    for (var i = 0, len = data.length; i < len; i++) {
      data[i] = (() => {
        if (data[i] === 0) {
          return data[i]
        }
        return data[i]
      })()
      delete data[i].$parent
      obj.push(deepClone(data[i]))
    }
  } else if (type === 'object') {
    for (var key in data) {
      delete data.$parent
      obj[key] = deepClone(data[key])
    }
  }
  return obj
}

/*
* 获取某个元素下标
*
*       arrays  : 传入的数组
*
*       obj     : 需要获取下标的元素
* */
export const contains = (arrays, obj) => {
  var i = arrays.length
  while (i--) {
    if (arrays[i].name === obj) {
      return i
    }
  }
  return false
}

/**
 * 设置px像素
 */
export const setPx = (val, defval = '') => {
  if (validatenull(val)) val = defval
  if (validatenull(val)) return ''
  val = val + ''
  if (val.indexOf('%') === -1) {
    val = val + 'px'
  }
  return val
}
