import { deepClone } from './util'

const phoneReg = /^1[23456789]\d{9}$/
const emailReg = /[a-zA-Z0-9]+@[a-zA-Z0-9]+.[a-zA-Z0-9]+/
const passwordReg = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&-_~#￥])[A-Za-z\d$@$!%*?&-_~#￥]{8,}/

export function checkPhone (phone) {
  return (phoneReg.test(phone))
}
export function checkEmail (email) {
  return (emailReg.test(email))
}
export function checkPassword (password) {
  return (passwordReg.test(password))
}
export const allReg = {
  phoneReg,
  emailReg,
  passwordReg
}
/**
 * 判断是否为空
 */
export function validatenull (val) {
  // 特殊判断
  if (val && parseInt(val) === 0) return false
  const list = ['$parent']
  if (typeof val === 'boolean') {
    return false
  }
  if (typeof val === 'number') {
    return false
  }
  if (val instanceof Array) {
    if (val.length === 0) return true
  } else if (val instanceof Object) {
    val = deepClone(val)
    list.forEach(ele => {
      delete val[ele]
    })
    if (JSON.stringify(val) === '{}') return true
  } else {
    if (
      val === 'null' ||
      val == null ||
      val === 'undefined' ||
      val === undefined ||
      val === ''
    ) {
      return true
    }
    return false
  }
  return false
}
