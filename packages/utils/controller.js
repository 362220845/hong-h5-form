export class Emitter {
  constructor () {
    this.eventReset()
  }

  // 监听事件重置
  eventReset () {
    if (this._eventListeners) {
      Object.keys(this._eventListeners).forEach(key => {
        delete this._eventListeners[key]
      })
    }
    this._eventListeners = {}
  }

  // 注册监听
  emit (funKey, params) {
    if (this._eventListeners && (this._eventListeners[funKey] instanceof Function)) {
      this._eventListeners[funKey](params)
    }
  }

  // 监听
  on (funKey, callback) {
    if (!funKey) {
      throw Error({
        message: 'event listener funkey undefined',
        callFunc: 'adapter:_on'
      })
    }
    if (!(callback instanceof Function)) {
      throw Error({
        message: 'event listener next param should be function',
        callFunc: 'adapter:_on'
      })
    }
    this._eventListeners[funKey] = callback
  }

  // 取消监听
  off (funKey) {
    if (!funKey) {
      throw Error({
        message: 'event listener funkey undefined',
        callFunc: 'adapter:_off'
      })
    }
    if (this._eventListeners[funKey]) {
      delete this._eventListeners[funKey]
    } else {
      throw Error({
        message: 'event listener unbind failed!',
        callFunc: 'adapter:_off'
      })
    }
  }
}
