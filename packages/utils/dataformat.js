// import { validatenull } from 'packages/utils/validate'
// import { detailDataType } from 'packages/utils/util'
import { getObjType } from './util'
import { validatenull } from './validate'
/**
 * 初始化数据格式
 */
export const initVal = ({ type, multiple, dataType, value, defaultValue }) => {
  let list = ![undefined, null].includes(value) ? value : (defaultValue)
  if (textIsArray(type, multiple)) {
    if (!Array.isArray(value)) {
      if (validatenull(value)) {
        list = defaultValue || []
      } else {
        list = (value || '').split(',') || []
      }
    }
    // 数据转化
    // list.forEach((ele, index) => {
    //   list[index] = detailDataType(ele, dataType)
    // })
  }
  return list
}

/**
 * 检验数据是否存在
*/
export const validText = ({ type, multiple, text }) => {
  if (textIsArray(type, multiple)) {
    return !!text.length
  } else {
    return !!text
  }
}

/**
 * 是否是多记录
*/
export const textIsArray = (type, multiple) => {
  return (['select'].includes(type) && multiple) || (['multi-record', 'check', 'tab-picker', 'multi-upload'].includes(type))
}

/**
 * 获取Tabbar Items
 */
export const getTabItems = (items) => {
  const _addItems = []
  items && items.forEach(item => {
    if (item.column) {
      item.column.forEach((column, index) => {
        const _index = index + 1
        _addItems.push({
          label: item.label + _index,
          name: item.name + '_' + _index,
          closable: item.closable,
          index
        })
      })
    } else {
      _addItems.push(item)
    }
  })
  return _addItems
}
/**
 * 获取TabForm格式数据
 */
export const getDealData = (form = {}) => {
  const _form = {}
  Object.keys(form).forEach(key => {
    const keyForm = form[key]
    const type = getObjType(keyForm)
    if (type === 'object') {
      _form[key] = keyForm
    }
    // array将处理成： tab_1
    if (type === 'array') {
      keyForm.forEach((item, index) => {
        _form[`${key}_${index + 1}`] = item
      })
    }
  })
  return _form
}
/**
 * 获取接口格式数据
 */
export const getRealData = (form) => {
  const _form = {}
  Object.keys(form).forEach(key => {
    if (key.includes('_')) {
      const t = key.split('_')
      const t0 = t[0]
      // const t1 = parseInt(t[1]) - 1
      !_form[t0] && (_form[t0] = [])
      _form[t0][_form[t0].length] = form[key]
    } else {
      _form[key] = form[key]
    }
  })
  return _form
}
