// ajax获取字典
export const sendDic = (params) => {
  const { url, query, method, props, format, responseFormat } = params
  return new Promise(resolve => {
    const callback = (data) => {
      if (typeof format === 'function') {
        data = format(data) || data
      }
      let list = []
      if (Object.keys(props).length) {
        list = data.map(item => {
          const { label, value, ...other } = item
          return {
            label: item[props.label],
            value: item[props.value],
            ...other
          }
        })
      } else {
        list = data
      }
      resolve(list)
    }
    if (!window.axios) {
      // packages.logs('axios')
      resolve([])
    }
    if (method.toLowerCase() === 'post') {
      window.axios.post(url, query).then(function (res) {
        let data = res.data.data || []
        if (typeof responseFormat === 'function') {
          data = responseFormat(res.data) || []
        }
        callback(data)
      }).catch(() => [
        resolve([])
      ])
    } else {
      window.axios.get(url, {
        params: query
      }).then(function (res) {
        let data = res.data.data || []
        if (typeof responseFormat === 'function') {
          data = responseFormat(res.data) || []
        }
        callback(data)
      }).catch(() => [
        resolve([])
      ])
    }
  })
}
