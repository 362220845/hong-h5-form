export default function () {
  return {
    data () {
      return {
        text: undefined,
        errorText: ''
      }
    },
    props: {
      change: Function,
      value: {},
      default: {},
      form: {},
      options: {
        type: Object,
        default: () => { return {} }
      },
      type: {
        type: String,
        default: 'field-item'
      },
      name: {
        type: String
      },
      title: {
        type: String
      },
      custom: {
        type: String,
        default: ''
      },
      append: {
        type: String,
        default: ''
      },
      isField: {
        type: Boolean,
        default: true
      },
      isSpace: {
        type: Boolean,
        default: false
      },
      display: {
        type: Boolean,
        default: true
      },
      props: {
        type: Object,
        default: () => {
          return {
            label: 'label',
            value: 'value'
          }
        }
      },
      emptyText: {
        type: String,
        default: '暂无数据'
      },
      format: [Function],
      labelFormat: [Function]
    },
    watch: {

    },
    computed: {

    },
    created () {

    },
    methods: {

    }
  }
}
