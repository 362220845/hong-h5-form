import { initVal, validText } from '@pkg/utils/dataformat'
export default function () {
  return {
    data () {
      return {
        text: undefined,
        errorText: ''
      }
    },
    props: {
      change: Function,
      blur: Function,
      value: {},
      default: {},
      type: {
        type: String,
        default: 'input'
      },
      name: {
        type: String
      },
      title: {
        type: String
      },
      brief: {
        type: String,
        default: ''
      },
      custom: {
        type: String,
        default: ''
      },
      maxlength: {
        type: [String, Number]
      },
      disabled: {
        type: Boolean,
        default: false
      },
      display: {
        type: Boolean,
        default: true
      },
      options: {
        type: Object,
        default: () => {}
      },
      form: {
        type: Object,
        default: () => {}
      },
      placeholder: {
        type: String
      },
      clearable: {
        type: Boolean,
        default: true
      },
      readonly: {
        type: Boolean,
        default: false
      },
      align: {
        type: String,
        default: 'right'
      },
      solid: {
        type: Boolean,
        default: false
      },
      // 特殊标记，如：是否为面签字段
      mark: {
        type: Boolean,
        default: false
      },
      rules: {
        type: Array,
        default: () => { return [] }
      },
      dicCache: {
        type: Object,
        default () { return {} }
      },
      dicData: {
        type: Array,
        default: () => { return [] }
      },
      dicUrl: {
        type: String,
        default: ''
      },
      dicMethod: {
        type: String,
        default: 'get'
      },
      dicParams: {
        type: Object,
        default () { return {} }
      },
      dicFormat: [Function],
      responseFormat: [Function],
      props: {
        type: Object,
        default: () => {
          return {}
        }
      },
      cascaderItem: {
        type: String,
        default: ''
      },
      dicReplaceKey: {
        type: [String, Number],
        default: ''
      },
      multiple: {
        type: Boolean,
        default: false
      },
      tag: {
        type: Boolean,
        default: false
      },
      isField: {
        type: Boolean,
        default: false
      }
    },
    watch: {
      value: {
        handler (val) {
          this.initVal()
        },
        immediate: true
      },
      text: {
        handler (value) {
          value !== undefined && this.handleChange(value)
          value && this.type !== 'multi-record' && this.valid()
        }
        // immediate: true
      },
      selectItem: {
        handler (value) {
          this.$emit('value-change', {
            handler: this.change,
            value: this.text,
            selectItem: value,
            cascaderItem: this.cascaderItem
          })
        }
      }
    },
    computed: {
      required () {
        return this.rules && !!this.rules.filter(item => item.required)[0]
      }
    },
    created () {
      this.text !== undefined && this.handleChange(this.text)
    },
    methods: {
      handleChange (value) {
        const result = value
        if (!['select', 'picker', 'tab-picker'].includes(this.type)) {
          this.$emit('value-change', {
            handler: this.change,
            value: result,
            selectItem: this.selectItem,
            cascaderItem: this.cascaderItem
          })
        }
        this.$emit('input', result)
      },
      valid (errorTip) {
        this.errorText = ''
        try {
          this.rules.forEach(rule => {
            const textIsExist = validText({ type: this.type, multiple: this.multiple, text: this.text })
            if (rule.required && !textIsExist) {
              let text = ['select', 'picker', 'radio', 'check', 'date', 'switch'].includes(this.type) ? '请选择' : '请输入'
              if (['upload', 'multi-upload'].includes(this.type)) text = '请上传'
              throw (rule.message || text + this.title)
            }
            if (textIsExist && rule.pattern && !rule.pattern.test(this.text)) {
              throw (rule.message || this.title + '格式不正确')
            }
            if (textIsExist && rule.validator && rule.validator(this.text)) {
              throw (rule.message || rule.validator(this.text))
            }
          })
        } catch (e) {
          if (errorTip) this.errorText = e
          return e
        }
      },
      initVal () {
        this.text = initVal({
          type: this.type,
          multiple: this.multiple,
          dataType: this.dataType,
          value: this.value,
          defaultValue: this.default
        })
      },
      onBlur () {
        // this.valid()
        // 暂只为input、textarea组件提供失焦回调
        if (['input', 'textarea'].includes(this.type)) {
          this.$emit('blur', {
            handler: this.blur,
            value: this.text
          })
        }
      }
    }
  }
}
