import Input from './input'
import Select from './select'
import Picker from './picker'
import Radio from './radio'
import Check from './check'
import Date from './date'
import Address from './address'
import Search from './search'
import Upload from './upload'
import Caption from './caption'
import DateDuring from './date-during'
import Textarea from './textarea'
import Switch from './switch'
import Button from './button'
import MultiRecord from './multi-record'
import MultiUpload from './multi-upload'
import TabPicker from './tab-picker'
import Slot from './form-slot'
// import Select from './select'
// import Checkbox from './checkbox'

const UIConfig = [
  Input,
  Caption,
  Select,
  Picker,
  Radio,
  Check,
  Date,
  Address,
  Search,
  Upload,
  DateDuring,
  Textarea,
  Switch,
  Button,
  MultiRecord,
  MultiUpload,
  TabPicker,
  Slot
  // Radio,
  // Checkbox
]
// const UIConfig = {
//   install (Vue) {
//     if (this.installed) return
//     this.installed = true

//     components.map(component => {
//       Vue.component(component.name, component)
//     })
//   }
// }

export default UIConfig
