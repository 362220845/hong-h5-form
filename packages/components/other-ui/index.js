import Grid from './grid'
import CountUp from './count-up'
import ScrollView from './scroll-view'

const UIConfig = [
  Grid,
  CountUp,
  ScrollView
]

export default UIConfig
