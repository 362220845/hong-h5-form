import FieldItem from './field-item'
import ValueField from './value-field'
import Recursion from './recursion'
import SingleRecursion from './single-recursion'
import Image from './image'
import ImageList from './image-list'
import CellItem from './cell-item'
import ValueTab from './value-tab'
import SubTitle from './sub-title'
import DetailSlot from './detail-slot'

const UIConfig = [
  FieldItem,
  ValueField,
  Recursion,
  SingleRecursion,
  Image,
  ImageList,
  CellItem,
  ValueTab,
  SubTitle,
  DetailSlot
]

export default UIConfig
