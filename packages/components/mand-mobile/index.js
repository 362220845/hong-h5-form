import {
  Button,
  Toast,
  InputItem,
  TextareaItem,
  CellItem,
  FieldItem,
  Field,
  ScrollView,
  ScrollViewMore,
  ScrollViewRefresh,
  Popup,
  CheckGroup,
  CheckBox,
  Check,
  TabBar,
  DatePicker,
  Icon,
  Dialog,
  ActionSheet,
  Picker,
  ImageViewer,
  TabPicker,
  ActivityIndicator,
  NoticeBar,
  Selector
} from 'mand-mobile'

const components = [
  Button,
  Toast,
  InputItem,
  TextareaItem,
  CellItem,
  FieldItem,
  Field,
  ScrollView,
  ScrollViewMore,
  ScrollViewRefresh,
  Popup,
  CheckGroup,
  CheckBox,
  Check,
  TabBar,
  DatePicker,
  Icon,
  Dialog,
  ActionSheet,
  Picker,
  ImageViewer,
  TabPicker,
  ActivityIndicator,
  NoticeBar,
  Selector
]
const MandConfig = {
  install (Vue) {
    if (this.installed) return
    this.installed = true

    components.map(component => {
      Vue.component(component.name, component)
    })

    Vue.prototype.$toast = Toast
    Vue.prototype.$dialog = Dialog
    Vue.prototype.$actionsheet = ActionSheet
  }
}

export default MandConfig
