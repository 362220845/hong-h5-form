import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import 'normalize.css'
// import mandMobile from 'mand-mobile'
import 'mand-mobile/lib/mand-mobile.css'
import '@/assets/custom.styl'

import HongH5Form from '../packages/index'
// Vue.use(mandMobile)
Vue.use(HongH5Form)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
