import request from '@/router/axios'
// 上传图片
export function uploadFile (query) {
  return request({
    url: '/fdfs/fdfsController?method=uploadFile',
    method: 'post',
    headers: {
      'Content-Type': 'multipart/form-data'
    },
    data: query
  })
}
/*
* 上传图片
* @param fileId
*/
export const downloadFileUrl = '/fdfs/fdfsController?method=downloadFile'
